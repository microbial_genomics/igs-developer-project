```
IGS Developer Project                                         o---O
.______       __  ___  __          __    _______      _______.O---o
|   _  \     |  |/  / |  |        |  |  /  _____|    /         O-o
|  |_)  |    |  '  /  |  |  ______|  | |  |  __     |   (----`  O
|      /     |    <   |  | |______|  | |  | |_ |     \   \     o-O
|  |\  \----.|  .  \  |  |        |  | |  |__| | .----)   |   o---O
| _| `._____||__|\__\ |__|        |__|  \______| |_______/    O---o
Assessment project for IGS developer applicants.               O-o
```

## Welcome 

Thank you for submitting your application for the developer position in the [IGS project](https://www.rki.de/DE/Content/Infekt/IGS/IGS_node.html). As part of our recruitment process, we would like you to solve a problem that requires your developer skills. This little project will help us to understand your skills and abilities better. Please refer to the readme for further instructions. Our team will review your results before your interview, and you will present your solution during the interview. We hope you enjoy this challenge and look forward to meeting you.

## The Problem

Genome sequencing of pathogens is a crucial aspect of the Integrated Genome Surveillance (IGS) program. To conduct further analysis, we require the preparation and extension of SARS-CoV-2 data to provide information about the geographical distribution of the participating sequencing laboratories. It is essential to have comprehensive coverage of the region that requires surveillance. Therefore, please establish a backend solution that includes a database and an API. The API will access this database for downstream systems.

## The Rules

There are only four rules:
+ You are working on a Linux machine
+ You are a Python developer
+ Use an SQL database or  [EdgeDB](https://www.edgedb.com/) 
+ Use [FastAPI](https://fastapi.tiangolo.com/)

## The Goal

+ Download SARS-CoV-2 metadata (SARS-CoV-2-Sequenzdaten_Deutschland.tsv.xz) from the [RKI SARS-CoV-2 Github repository](https://github.com/robert-koch-institut/SARS-CoV-2-Sequenzdaten_aus_Deutschland)
+ Use the column SL.POSTAL_CODE containing the sequencing lab's zip code and extend it with state, county, longitude, and latitude (this data is not part of the metadata. Be creative.) 
+ Clean the data, find faulty entries, and write them to log
+ Develop a data model for the database
+ Write everything into an SQL or [EdgeDB](https://www.edgedb.com/) database
+ Create a daily job that runs at 07:06 to update the database
+ (optional) Plot a map with the distribution of the labs
+ (optional) Implement data privacy measures to ensure sensitive information is protected
+ (optional) Conduct regular data backups to avoid data loss in case of system failures.
+ (optional) Unit test your code with [pytest](https://docs.pytest.org/en/7.4.x/) or a testing framework of your choice
+ (optional) Make a singularity container out of it
+ (optional) Go wild and do other stuff
+ Fork this to a private repository in your namespace on GitLab and invite @HartkopfF as a member with developer permissions
+ Create one single slide describing your project

## Feedback

Don't hesitate to contact me for feedback or questions at hartkopff(at)rki.de .
